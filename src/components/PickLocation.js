import React, { Component } from "react";
import {
  View,
  Button,
  StyleSheet,
  Dimensions, 
  Modal,
  Text
} from "react-native";
import MapView from "react-native-maps";

class PickLocation extends Component {
  state = {
    focusedLocation: {
      latitude: 37.7900352,
      longitude: -122.4013726,
      latitudeDelta: 0.0122,
      longitudeDelta:
        Dimensions.get("window").width /
        Dimensions.get("window").height *
        0.0122
    },
    locationChosen: false,
    savedLocation: [],
    isModalVisible: false,
  };

  toggleModal = () => {
    this.setState({isModalVisible: !this.state.isModalVisible});
  };

  pickLocationHandler = event => {
    const coords = event.nativeEvent.coordinate;
    this.map.animateToRegion({
      ...this.state.focusedLocation,
      latitude: coords.latitude,
      longitude: coords.longitude
    });
    this.setState(prevState => {
      return {
        focusedLocation: {
          ...prevState.focusedLocation,
          latitude: coords.latitude,
          longitude: coords.longitude
        },
        locationChosen: true
      };
    });
  };

  getLocationHandler = () => {
    navigator.geolocation.getCurrentPosition(pos => {
      const coordsEvent = {
        nativeEvent: {
          coordinate: {
            latitude: pos.coords.latitude,
            longitude: pos.coords.longitude
          }
        }
      };
      this.pickLocationHandler(coordsEvent);
    },
    err => {
      console.log(err);
      alert("Fetching the Position failed, please pick one manually!");
    })
  }

  saveLocation = () => {
    this.setState(prevState => {
      return {
        focusedLocation: prevState.focusedLocation,
        savedLocation: [ 
          ...prevState.savedLocation,
          <MapView.Marker coordinate={this.state.focusedLocation}/> ]
      };
    });
  };

  render() {
    let marker = null;

    if (this.state.locationChosen) {
      marker = <MapView.Marker coordinate={this.state.focusedLocation} />;
    }

    return (
      <View>
        <View style={styles.container}>
        <MapView
          showsUserLocation={true}
          initialRegion={this.props.coordinate}
          style={styles.map}
          onPress={this.pickLocationHandler}
          ref={ref => this.map = ref}
        >

        {this.state.savedLocation.map(marker => (
          <MapView.Marker
            coordinate={marker.props.coordinate}
          />
        ))}

        {marker}

        </MapView>
        <View style={styles.button}>
          <View>
              <Button 
                title="Locate Me" 
                onPress={this.getLocationHandler} />
          </View>
          <View>
            <Button 
              title="Save Location" 
              onPress={this.saveLocation} />
          </View>
        </View>
        <View style={styles.menuButton}>
          <Button 
          title="Menu" 
          onPress={this.toggleModal} />
        </View>
        </View>
        {/* <Modal isVisible={this.state.isModalVisible}>
          <View>
            <Text>Hello!</Text>
            <Button title="Hide modal" onPress={this.toggleModal} />
          </View>
        </Modal> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
  },
  button: {
    position: 'absolute',
    backgroundColor: "black",
    borderRadius: 5,
    bottom: '5%', 
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  map: {
    flex: 1,
  },
  menuButton: {
    position: 'absolute',
    backgroundColor: "black",
    borderRadius: 5,
    top: '5%', 
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});

export default PickLocation;

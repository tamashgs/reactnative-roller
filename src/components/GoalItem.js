import React from 'react'
import { View, Text, TouchableNativeFeedbackBase, TouchableOpacity } from 'react-native'

const GoalItem = props => {
    return (
        <TouchableOpacity onPress={props.onDelete.bind(this, props.id)}>
            <View>
                <Text>{props.title}</Text>
            </View>
        </TouchableOpacity>
    );
};

export default GoalItem;
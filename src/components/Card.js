import React from 'react'
import {View, StyleSheet, TouchableOpacity} from 'react-native'

const Card = props => {
    return <TouchableOpacity onPress={props.onPress}>
        <View style={
            {...style.card,
            ...props.style}
        }>
        {props.children}
        </View>
    </TouchableOpacity>
}

const style = StyleSheet.create({
    card: {
        // width: 300,
        // maxWidth: '80%',
        alignItems: 'center',
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 2},
        shadowRadius: 10,
        shadowOpacity: 0.5,
        backgroundColor: 'white',
        elevation: 5,
        padding: 20,
        borderRadius: 10,
        marginTop: 20
    }
});
 
export default Card
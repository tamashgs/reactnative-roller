import React, {useState}from 'react'
import {View, TextInput, Button} from 'react-native'

const GoalInput = props => {
    const [enteredText, setEnteredText] = useState('');

    const inputHandler = text => {
        setEnteredText(text);
    }

    return (
        <View style = {{flexDirection: 'row', justifyContent: "space-between", alignItems: "center"}}>
            <TextInput 
                placeholder="Kocsog" 
                style = {{ width: '80%', borderColor: 'blue', borderWidth: 2, padding: 10}}
                onChangeText = { inputHandler }
                value = { enteredText }
            />
            <Button title="ADD" onPress = {props.onAddGoal.bind(this, enteredText) }/>
        </View>
    );
};

export default GoalInput;
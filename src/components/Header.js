import React from 'react'
import { StyleSheet, View, Text } from 'react-native'

const Header = props => {
    return (
        <View style={styles.header}>
            <Text style={styles.headerTitle}>{props.title}</Text>
        </View>
    )
};

const styles = StyleSheet.create({
    header: {
        width: '100%',
        height: 100,
        paddingTop: 25,
        backgroundColor: 'blue',
        alignItems: 'center',
        justifyContent: 'center'
    },
    headerTitle: {
        fontSize: 28,
        color: 'black'
    }
});

export default Header;
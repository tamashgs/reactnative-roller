import React, { useState } from 'react';
import { View, FlatList } from 'react-native';

import GoalItem from './GoalItem'
import GoalInput from './GoalInput'

const Input = props => {
  const [outputText, setOutputText] = useState('');

  const addText = goalTitle => {
    setOutputText(outputText => [
      ...outputText, 
      { id: Math.random().toString(), value: goalTitle}
    ]); 
  }

  const removeText = textId => {
    setOutputText(outputText => {
      return outputText.filter((goal) => goal.id !== textId )
    })
  }

  return (
    <View style = {{padding: 50}}>
      <GoalInput onAddGoal = {addText}/>
      <FlatList 
        keyExtractor = {(item, index) => item.id}
        data = {outputText}
        renderItem = { itemData => 
          <GoalItem 
            id = {itemData.item.id}
            onDelete = {removeText} 
            title = {itemData.item.value} 
          />
        }
       />
    </View>
  );
}

export default Input;
import React, {Component} from 'react'
import { View, Text, StyleSheet, Button, AsyncStorage } from 'react-native'
import MenuDrawer from 'react-native-side-drawer'
import Card from '../components/Card'
import Map from './Map'
import AdminMap from './AdminMap'
import { AuthContext } from "./context.js";

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      username: ""
    };
  }

  toggleOpen = () => {
    this.setState({ open: !this.state.open });
  };

  componentDidMount(){
    this.getUsername()
  }

  async getUsername(){
    this.state.username = await AsyncStorage.getItem("username");
  }

  drawerContent = () => {
    return (
      <View style={styles.animatedBox}>
        <Text style={{marginTop: 30, fontSize: 30}}>Hello {this.state.username}</Text>
        <Card onPress={this.toggleOpen}>
          <Text>Close</Text>
        </Card>
        <Card onPress={() => this.props.navigation.navigate('Profile')}>
          <Text>Profile</Text>
        </Card>
        <Card>
          <Text>History</Text>
        </Card>
        <Card onPress={() => this.props.navigation.navigate('Setting')}>
          <Text>Setting</Text>
        </Card>
        <Card onPress={AuthContext._currentValue.signOut}>
          <Button title="Sign Out" />
        </Card>
      </View>
    );
  };

  render() {
    return (
      <View style={{marginTop: -5}}>
        {Platform.OS === 'ios' ?
          <MenuDrawer 
            open={this.state.open} 
            drawerContent={this.drawerContent()}
            drawerPercentage={60}
            animationTime={250}
            overlay={false}
            opacity={0.5}
          >
            <Map onMenuPress={this.toggleOpen}/>
          </MenuDrawer>
        :
        <Map onMenuPress={AuthContext._currentValue.signOut}/>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  animatedBox: {
    flex: 1,
    textAlign: 'center',
    backgroundColor: "seagreen",
    padding: 10,
    opacity: 50,
  },
})
import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'

export default LoadingScreen = () => {
    return (
        <View style={styles.center}>
            {/* <Text>Loading...</Text> */}
            <Image
                style={{width: 80, height: 80}}
                source={require('../../assets/loading.gif')}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    center: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(1, 1, 1, 0)'
    },
  })
import axios from 'axios';
import { URL } from './config.js'

export const api = axios.create({
    baseURL: URL
});

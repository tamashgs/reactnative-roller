import  React from 'react';
import { View, TextInput, Button, StyleSheet } from 'react-native'
import { AuthContext } from './context.js';

export const SignInScreen = ({navigation}, error) => {
  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');

  const { signIn } = React.useContext(AuthContext);
  
  let passInput;

  return (
    <View style={styles.container}>
      <TextInput
        placeholder="Username"
        value={username}
        onChangeText={setUsername}
        style={styles.input}
        returnKeyType={'next'}
        onSubmitEditing={() => passInput.focus()}
      />
      <TextInput
        placeholder="Password"
        value={password}
        onChangeText={setPassword}
        style={styles.input}
        secureTextEntry
        ref={pass => passInput = pass}
        enablesReturnKeyAutomatically={true}
        returnKeyType={'send'}
        onSubmitEditing={() => signIn({ username, password })}
      />
      {/* TODO */}
      {error.type != null ? <Text>invalid</Text> : null}
      <Button 
        title="Sign in" 
        style={styles.button}
        onPress={() => signIn({ username, password })} />
      <Button
        title="Create Account"
        style={styles.button}
        onPress={() => navigation.push("SignUp")}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    // backgroundColor: '#0D0D0D'
  },
  input: {
    backgroundColor: 'grey',
    height: '10%',
    width: '80%',
    margin: 5,
    borderRadius: 30,
    textAlign: 'center'
  },
  button: {
    height: '10%',
    width: '80%',
    borderRadius: 80,
    margin: 5,
  }
});
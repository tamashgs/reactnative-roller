import React, { Component } from 'react';
import { View, StyleSheet, AsyncStorage, Alert } from 'react-native';
import {
    Caption,
    Paragraph,
    Text,
    Drawer,
    Avatar,
    Title,
    TouchableRipple,
    Switch
} from 'react-native-paper';
import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { color } from 'react-native-reanimated';
import { api } from './api.js';

import{ AuthContext } from './context';

export class DrawerContent extends Component{
    constructor(props){
        super(props)
        this.state = {
            username: null,
            userToken: null,
            rides: 0,
            time: 0
        };
        this.getUserInfoFromAsyncStorage().then(() =>
            this.getUserData()
        )
    }

    getUserInfoFromAsyncStorage = async () => {
        this.state.username = await AsyncStorage.getItem("username");
        this.state.userToken = await AsyncStorage.getItem("userToken");
    }

    msToTime(duration) {
        let seconds = Math.floor((duration / 1000) % 60),
            minutes = Math.floor((duration / (1000 * 60)) % 60),
            hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

        hours = (hours < 10) ? "0" + hours : hours;
        minutes = (minutes < 10) ? "0" + minutes : minutes;
        seconds = (seconds < 10) ? "0" + seconds : seconds;

        return hours + ":" + minutes + ":" + seconds;
    }
      
    getUserData = () => {
        const config = { 
            headers: {
              'Authorization': this.state.userToken,
            }
        }
        api.get(`user/tamas/history`, config)
        .then(response => {
            let time = 0;
            response.data.forEach(element => {
                if (element.endTime != null && element.startTime != null)
                    time += element.endTime - element.startTime
            })
            this.setState(state => {
                return {
                    ...state,
                    rides: response.data.length,
                    time: this.msToTime(time)
                }
            })
        })
        .catch(error => {
            if ( error.response ) {
                console.log(error.response.data)
                Alert.alert(error.response.data.message);
            } else {
                console.log(error)
            }
        });
    }

    // componentDidMount(){
    //     const paperTheme = useTheme();
    //     const { signOut, toggleTheme } = React.useContext(AuthContext);
    // }

    render() { 
        return (
        <View style={{flex:1}}>
            <DrawerContentScrollView {...this.props}>
                <View style={styles.drawerContent}>
                    <View style={styles.user}>
                        <Avatar.Image
                            source={{
                                uri: 'https://kansai-resilience-forum.jp/wp-content/uploads/2019/02/IAFOR-Blank-Avatar-Image-1.jpg'
                            }}
                            size={75}
                        />
                        <View><Title>
                            {this.state.username}
                        </Title></View>
                        <View style={styles.row}>
                            <View style={styles.section}>
                                <Caption style={styles.caption}>Rides </Caption>
                                <Paragraph style={[styles.paragraph, styles.caption]}>{this.state.rides}</Paragraph>
                            </View>
                            <View style={styles.section}>
                                <Caption style={styles.caption}>Time </Caption>
                                <Paragraph style={[styles.paragraph, styles.caption]}>{this.state.time}</Paragraph>
                            </View>
                        </View>
                     </View>
                    <Drawer.Section  style={styles.drawerSection}>
                        <DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                name="map" 
                                color={color}
                                size={30}
                                />
                            )}
                            label="Map"
                            onPress={() => {this.props.navigation.navigate('Map')}}
                        />
                        <DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                name="account-outline" 
                                color={color}
                                size={30}
                                />
                            )}
                            label="Profile"
                            onPress={() => {this.props.navigation.navigate('Profile')}}
                        />
                        <DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                name="settings-outline" 
                                color={color}
                                size={30}
                                />
                            )}
                            label="Settings"
                            onPress={() => {this.props.navigation.navigate('Setting')}}
                        />
                        <DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                name="account-check-outline" 
                                color={color}
                                size={30}
                                />
                            )}
                            label="Support"
                            onPress={() => {this.props.navigation.navigate('Support')}}
                        />
                    </Drawer.Section>
                    <TouchableRipple style={styles.bottomDrawerSection}>
                        <View style={styles.preference}>
                            <Text>Dark Theme</Text>
                            <View pointerEvents="none">
                                <Switch/>
                            </View>
                        </View>
                    </TouchableRipple>
                </View>
            </DrawerContentScrollView>
            <Drawer.Section style={styles.bottomDrawerSection}>
                 <DrawerItem 
                    icon={({color, size}) => (
                        <Icon 
                        name="exit-to-app" 
                        color={color}
                        size={size}
                        />
                    )}
                    label="Sign Out"
                    onPress={AuthContext._currentValue.signOut}
                />
            </Drawer.Section>
        </View>
        );
    }
}

const styles = StyleSheet.create({
    drawerContent: {
      flex: 1,
    },
    user: {
      padding: 20,
      borderTopColor: '#f4f4f4',
      borderTopWidth: 1,
      borderBottomColor: '#f4f4f4',
      borderBottomWidth: 1,
      alignItems: 'center'
    },
    title: {
      fontSize: 16,
      marginTop: 3,
      fontWeight: 'bold',
    },
    caption: {
      fontSize: 14,
      lineHeight: 14,
    },
    row: {
      marginTop: 20,
      flexDirection: 'row',
      alignItems: 'center',
    },
    section: {
      flexDirection: 'row',
      alignItems: 'center',
      margin: 10
    },
    paragraph: {
      fontWeight: 'bold',
      marginRight: 3,
    },
    drawerSection: {
      marginTop: 15,
      marginBottom: 15,
      fontSize: 30,
    },
    bottomDrawerSection: {
        justifyContent: 'center',
        margin: 1,
    },
    preference: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingVertical: 12,
      paddingHorizontal: 16,
      paddingRight: 0,
    },
  });
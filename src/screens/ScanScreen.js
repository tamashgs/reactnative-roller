import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, Button, Alert  } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export const ScanScreen = props => {
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);

  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);

  const handleBarCodeScanned = ({ type, data }) => {
    setScanned(true);
    props.onScanned(data);
    // alert(`Bar code with type ${type} and data ${data} has been scanned!`);
  };

  const test = () => {
    props.onScanned('5ea869c28e05681f164e1366')
  }

  const flashlight = () => {
    Alert.alert("Aszitted mi? :DDD");
  }

  if (hasPermission === null) {
    return (
      <View style={styles.scannerContainer}>
        <Text>Requesting for camera permission</Text>
      </View>
    )
  }

  if (hasPermission === false) {
    return (
      <View style={styles.scannerContainer}>
        <Text>No access to camera</Text>
      </View>
    )
  }

  return (
    <View
      style={styles.scannerContainer}>
      <BarCodeScanner
        onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
        style={StyleSheet.absoluteFillObject}
      />
      <Icon
        style={styles.close}
        name="close"
        size={40}
        color='grey'
        onPress={props.onClose}
      />
      <Icon
        style={styles.flashlight}
        name="flashlight"
        size={40}
        color='grey'
        onPress={flashlight}
      />
      <Button title="test" onPress={test}/>
      {scanned && (
        <Button title={'Tap to Scan Again'} 
          onPress={() => setScanned(false)} 
        />
      )}
    </View>
  );

}

const styles = StyleSheet.create({
  scannerContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 20,
    borderRadius: 50,
    position: 'absolute'
  },
  flashlight: {
    bottom: 10,
    left: 10,
    position: 'absolute'
  },
  close: {
    top: 10,
    right: 10,
    position: 'absolute'
  }
})

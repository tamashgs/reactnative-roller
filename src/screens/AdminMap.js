// import React, { Component } from "react";
// import {
//   View,
//   Button,
//   StyleSheet,
//   Dimensions,
//   Text,
//   Image,
//   Alert
// } from "react-native";
// import MapView, { Callout } from "react-native-maps";
// import { Icon } from 'react-native-elements'
// import Carousel from 'react-native-snap-carousel';
// import api from '../config/api.js';


// export default class MapScreen extends Component {
//   state = {
//     focusedLocation: {
//       latitude: 37.7900352,
//       longitude: -122.4013726,
//       latitudeDelta: 0.0122,
//       longitudeDelta:
//         Dimensions.get("window").width /
//         Dimensions.get("window").height *
//         0.0122
//     },
//     locationChosen: false,
//     itemLocations: [],
//     isModalVisible: false,
//   };

//   pickLocationHandler = event => {
//     const coords = event.nativeEvent.coordinate;
//     this.map.animateToRegion({
//       ...this.state.focusedLocation,
//       latitude: coords.latitude,
//       longitude: coords.longitude
//     });
//     this.setState(prevState => {
//       return {
//         focusedLocation: {
//           ...prevState.focusedLocation,
//           latitude: coords.latitude,
//           longitude: coords.longitude
//         },
//         locationChosen: true
//       };
//     });
//   };

//   getLocationHandler = () => {
//     navigator.geolocation.getCurrentPosition(position => {
//       this.map.animateToRegion({
//         ...this.state.focusedLocation,
//         longitude: position.coords.longitude,
//         latitude: position.coords.latitude
//       });
//     },
//     err => {
//       console.log(err);
//       alert("Fetching the Position failed, please pick one manually!");
//     })
//   }
 
//   componentDidMount() {  
//     api.get('/locations')
//       .then(res =>  {
//         res.data.forEach(element => {
//           this.setState(prevState => {
//             return {
//               itemLocations: [ 
//                 ...prevState.itemLocations,
//                 element
//               ],
//             }
//           })
//         })
//       })
//     .catch(error => Alert.alert(error))
//   }

//   saveLocation = () => {
//     api.post('/locations', this.state.focusedLocation)
//     .then(res => console.log(res))
//     .catch(error => console.log(error))
//   };

//   menuPressed = () => {
//     this.props.onMenuPress();
//   }

//   onMarkerPressed = (index) => {
//     this._carousel.snapToItem(index)
//   }

//   onCarouselItemChanged = (index) => {
//     this.map.animateToRegion(this.state.itemLocations[index]);
//   }

//   renderCarouselItem = ({item}) => {
//     return (
//       <View style={styles.carouselItem}>
//         <Image style={styles.carouselPhoto} source={require('./bike.png')} />
//         <Text>Name: Dzsalóka</Text>
//         <Text>Price: 0.01 RON/S</Text>
//         <Text>{item.latitude}</Text>
//         <Text>{item.longitude}</Text>
//         <Button title='Reserve' onPress={ () => Alert.alert("Kérjél biciklit anyádtól!")}/>
//         <Button title='Navigate' onPress={ () => Alert.alert("Fogyatékos vagy, bazd meg? Ott van elötted, ha nem találod meg inkább ne bicikliz!")}/>
//       </View>
//     )
//   }

//   render() { 
//     return (
//       <View>
//         <View style={styles.container}>
//           <MapView style={styles.map}
//             showsUserLocation={true}
//             initialRegion={this.props.coordinate}
//             onPress={this.pickLocationHandler}
//             ref={ref => this.map = ref}
//           >
//             {this.state.itemLocations.map(markerLocation => (    
//               <MapView.Marker
//                 key={markerLocation.index}
//                 coordinate={markerLocation}
//                 onPress={ () => this.onMarkerPressed(markerLocation.index)}
//               >
//                 <Callout>
//                   <Text>{markerLocation.longitude}</Text>
//                   <Text>{markerLocation.latitude}</Text>
//                 </Callout>
//               </MapView.Marker>
//             ))}
//             {this.state.locationChosen ? <MapView.Marker coordinate={this.state.focusedLocation}/> : null}
//           </MapView>
//           <View style={styles.button}>
//             <Button 
//               title="Save Location" 
//               onPress={this.saveLocation} />
//           </View>
//           <View style={styles.locate}>
//             <Icon name="my-location" size={40} onPress={this.getLocationHandler} />
//           </View>
//           <View style={styles.menu}>
//             <Icon name="menu" size={40} onPress={() => this.menuPressed()} />
//           </View>
//           <View style={styles.carousel}>
//             <Carousel
//               ref={(c) => { this._carousel = c; }}
//               data={this.state.itemLocations}
//               renderItem={this.renderCarouselItem}
//               sliderWidth={Dimensions.get('window').width}
//               itemWidth={300}
//               onSnapToItem={this.onCarouselItemChanged}
//             />
//           </View>
//         </View>
//       </View>
//     )
//   }
// }

// const styles = StyleSheet.create({
//   map: {
//     flex: 1,
//   },
//   container: {
//     width: "100%",
//     height: "100%",
//   },
//   carousel: {
//     position: 'absolute',
//     bottom: '15%',
//     alignSelf: 'center',
//   },
//   carouselItem: {
//     alignItems: 'center',
//     backgroundColor: 'white',
//     padding: 20,
//     borderRadius: 20,
//     marginTop: 100
//   },
//   carouselPhoto: {
//     top: -100,
//     width: 150,
//     height: 150,
//     position: "absolute"
//   },
//   locate:{
//     position: 'absolute',
//     bottom: '5%',
//     right: '10%'
//   },
//   menu: {
//     position: 'absolute',
//     top: '5%',
//     left: '5%',
//   },
//   button: {
//     position: 'absolute',
//     backgroundColor: "black",
//     borderRadius: 5,
//     bottom: '5%', 
//     alignSelf: 'center',
//     flexDirection: 'row',
//     justifyContent: 'space-between'
//   }
// });

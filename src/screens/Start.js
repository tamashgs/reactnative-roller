import React from 'react';
import { View, Text, StyleSheet, TextInput, Button } from 'react-native';
import Header from '../components/Header'
import Card from '../components/Card'

const StartScreen = props => {
    return (
        <View style = {{flex: 1}}>
            <Header title="Guess a number"/>
            <View style = {styles.screen}>
                <Card style={styles.input}>
                    <Text>Select a number</Text>
                    <TextInput
                        keyboardType = 'numeric'
                        maxLength = '2'
                        style={{height: 30, width: 80, backgroundColor: 'grey'}}
                    />
                    <View style={styles.button}>
                        <View style={{width: 90}}>
                            <Button  title="Reset" onPress={()=>{}} color='red' />
                        </View>
                        <View style={{width: 90}}>
                            <Button title="Confirm" onPress={()=>{}} color='blue'/>
                        </View>
                    </View>
                </Card>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        alignItems: 'center',
        padding: 10,
    },
    input: {
        width: 300,
        maxWidth: '80%',
        alignItems: 'center',
    },
    button: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
        paddingHorizontal: 15
    }
});

export default StartScreen
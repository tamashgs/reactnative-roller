import React from 'react'
import { View, TextInput, Button, Modal, Text, StyleSheet } from 'react-native'
import { AuthContext } from './context.js';

export const CreateAccount = ({navigation}) => {
    const [username, setUsername] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [password2, setPassword2] = React.useState('');
    const [modalVisible, setModalVisible] = React.useState(false);
    const [modalType, setModalType] = React.useState('done');
  
    const { signUp } = React.useContext(AuthContext);
  
    const openModal = (type) => {
      setModalType(type);
      setModalVisible(true);
    }
  
    const closeModal = () => setModalVisible(false);
  
    return (
      <View style={{flex: 1}}>
        <View style={styles.container}>
          <TextInput
            placeholder="Username"
            value={username}
            onChangeText={setUsername}
            style={styles.input}
          />
          <TextInput
            placeholder="Email"
            keyboardType="email-address"
            value={email}
            onChangeText={setEmail}
            style={styles.input}
          />
          <TextInput
            placeholder="Password"
            value={password}
            onChangeText={setPassword}
            style={styles.input}
            secureTextEntry
          />
          <TextInput
            placeholder="Password"
            value={password2}
            onChangeText={setPassword2}
            style={styles.input}
            secureTextEntry
          />
          <Button 
            title="Sign Up" 
            onPress={() => signUp({
              username, email, password, password2, 
              openModal, closeModal})} />
        </View>
        <Modal animationType="slide" visible={modalVisible}>
          {modalType == 'done' ? 
          <View style={styles.modal}>
            <Text>Account successfully created</Text>
            <Button title="Sign in" onPress={() => navigation.navigate("SignIn")} />
          </View>
          :
          <LoadingScreen/>
          }
        </Modal>  
    </View>
    );
  };

  const styles = StyleSheet.create({
    modal: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
    },
    container: {
      flex: 1,
      alignItems: 'center',
    },
    input: {
      backgroundColor: 'grey',
      height: '10%',
      width: '80%',
      margin: 5,
      borderRadius: 30,
      textAlign: 'center'
    },
    button: {
      height: '10%',
      width: '80%',
      borderRadius: 80,
      margin: 5,
    }
  });
  
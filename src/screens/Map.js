import React, { Component } from "react";
import {
  View,
  Button,
  StyleSheet,
  Dimensions,
  Text,
  Image,
  Alert,
  AsyncStorage,
  Modal
} from "react-native";
import MapView, { Callout } from "react-native-maps";
import { Icon } from 'react-native-elements'
import Carousel from 'react-native-snap-carousel';
import MapViewDirections from 'react-native-maps-directions';
import { api } from './api.js';
import { URL, GOOGLE_MAPS_APIKEY } from './config.js'
import io from 'socket.io-client';
import { ScanScreen } from './ScanScreen.js'

export default class MapScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      focusedLocation: {
        latitudeDelta: 0.01,
        longitudeDelta: 0.01
      },
      itemLocations: [],
      selectedItem: null,
      username: null,
      rentTime: null,
      direction: null,
      location: null,
      scanScreen: false,
      rentCode: null,
      userToken: null
    };
  }
  
  initStateFromAsyncStorage = () => {
    Promise.all([
      AsyncStorage.getItem("username"),
      AsyncStorage.getItem("rentTime"),
      AsyncStorage.getItem("userToken")
    ]).then(array => this.setState(state => {
      return {
        ...state,
        username: array[0],
        rentTime: array[1],
        userToken: array[2]
      }
    }));
  }

  //NE filter csak ne renderelje
  initSocketSubscription = () => {
    this.socket = io(URL);
    this.socket.on('setUnavailable', itemID => {
      this.setState(state => {
        return {
          ...state,
          itemLocations: state.itemLocations.filter( item => item._id != itemID)
        }})
    });
    this.socket.on('setAvailable', () => this.getLocationsFromServer());
  }

  getLocationsFromServer = () => {
    api.get('/locations')
      .then(res => {
        let index = -1;
        const data = res.data
          .map(item => {
              index += 1;
              return {...item, index: index}
          })
          .filter(item => item.available == true);
        this.setState(prevState => {
          return {
            ...prevState,
            itemLocations: data
          }
        })
      })
      .catch(error => {
        if ( error.response ) {
            console.log(error.response.data)
            Alert.alert(error.response.data.message);
        } else {
            console.log(error)
        }
      });
  }

  // Optimalizala: csak egyszer állítani state-et!
  componentDidMount() {
    this.initStateFromAsyncStorage();
    this.initSocketSubscription();
    this.getLocationsFromServer();
  }

  componentWillUnmount() {
    this.socket.removeAllListeners();
  }

  getLocationHandler = () => {
    navigator.geolocation.getCurrentPosition(position => {
      this.map.animateToRegion({
        ...this.state.focusedLocation,
        longitude: position.coords.longitude,
        latitude: position.coords.latitude
      });
    },
    err => {
      console.log(err);
      alert("Fetching the Position failed, please pick one manually!");
    })
  }

  menuPressed = () => {
    this.props.navigation.openDrawer();
    // this.props.onMenuPress();
  }

  onMarkerPressed = index => this._carousel.snapToItem(index)

  onCarouselItemChanged = index => { 
    const region = {...this.state.itemLocations[index],
      latitude: this.state.itemLocations[index].latitude - 0.002}
    this.map.animateToRegion(region)
  }

  onRentPressed = id => {
    this.setState(state => {
      return {
        ...state,
        selectedItem: id
      }
    })
    this.toggleScanner(); 
  }

  //todo - szervertol
  scannerHandler = scanned => {
    if ( this.state.selectedItem == scanned ) {
      this.toggleScanner();
      this.state.rentCode = '9934';
      this.startRent(this.state.selectedItem);
    } else {
      Alert.alert('Invalid QR code!');
    }
  }

  toggleScanner = () => this.setState(state => {
    return {
      ...state,
      scanScreen: !state.scanScreen,
    }
  })

  startRent = id => {
    const URL = `/rent/locations/${id}/users/${this.state.username}/start`
    const data = null
    const config = { 
      headers: {
        'Authorization': this.state.userToken,
      }
    }
    api.patch(URL, data, config)
    .then((response) => {
      this.socket.emit('reserve', id);
      this.setState(state => {
        return {
          ...state,
          rentTime: response.data.rentTime
        }
      })
      AsyncStorage.setItem("rentTime", response.data.rentTime.toString());
    })
    .catch(error => {
      console.log(error.response.data)
    });
    // Alert.alert("Kérjél biciklit anyádtól!");
  }

  endRent = () => {
    const URL = `/rent/${this.state.username}/stop`
    const data = null
    const config = { 
      headers: {
        'Authorization': this.state.userToken,
      }
    }
    api.patch(URL, data, config)
    .then((response) => {
      this.socket.emit('cancel', response.data.rentId);
      this.setState(state => {
        return {
          ...state,
          rentTime: null
        }
      })
      AsyncStorage.removeItem("rentTime");
    })
    .catch(error => {
      console.log(error.response.data)
    });
  }

  getDirection = item => {
    const direction = { latitude: item.latitude, longitude: item.longitude };
    if (this.state.direction === direction) direction = null
    this.setState(state => {
      return {
        ...state,
        direction: direction }
    })
    navigator.geolocation.getCurrentPosition(position => {
      const location = { latitude: position.coords.latitude, longitude: position.coords.longitude}
      this.setState(state => {
        return {
          ...state,
          location: location }
      })
    })
    // Alert.alert("Fogyatékos vagy, bazd meg? Ott van elötted, ha nem találod meg inkább ne bicikliz!")
  }

  renderCarouselItem = ({item, index}) => {
    return (
      <View style={styles.carouselItem} id={item._id}>
        <Image style={styles.carouselPhoto} source={require('../../assets/bike.png')} />
        <Text>Name: {item.name}</Text>
        <Text>Price: {item.price} RON/min</Text>
        <Text>{item.latitude}</Text>
        <Text>{item.longitude}</Text>
        <View style={{width: '100%', flexDirection: 'row', justifyContent: 'space-around', alignContent: 'center'}}>
          <Button title='Rent' onPress={() => this.onRentPressed(item._id)}/>
          <Button title='Navigate' onPress={ () => this.getDirection(item)}/>
        </View>
      </View>
    )
  }

  render() { 
    const date = new Date(this.state.rentTime * 1000);
    const time = `${date.getHours()}:${date.getMinutes()}`
    return (
      <View>
        <View style={styles.container}>
          <MapView 
            style={styles.map}
            showsUserLocation={true}
            initialRegion={this.props.coordinate}
            ref={ref => this.map = ref}
          >
            {this.state.itemLocations.map(markerItem => (    
              <MapView.Marker
                key={markerItem._id}
                index={markerItem.index}
                coordinate={markerItem}
                onPress={() => this.onMarkerPressed(markerItem.index)}
              >
                <Callout>
                  <Text>{markerItem.name}</Text>
                  <Text>{markerItem.price}</Text>
                  <Text>{markerItem.longitude}</Text>
                  <Text>{markerItem.latitude}</Text>
                </Callout>
              </MapView.Marker>
            ))}
            { this.state.direction != null ?
              <MapViewDirections
              origin={this.state.location}
              destination={this.state.direction}
              apikey={GOOGLE_MAPS_APIKEY}
              strokeWidth={5}
              strokeColor="dodgerblue"
            /> : null
            }
          </MapView>
          <View style={styles.button}>
            <Button title="Save Location" onPress={this.saveLocation} />
          </View>
          <View style={styles.locate}>
            <Icon name="my-location" size={40} onPress={this.getLocationHandler} />
          </View>
          <View style={styles.menu}>
            <Icon name="menu" size={40} onPress={() => this.menuPressed()} />
          </View>
          <View style={styles.carousel}>
            <Carousel
              ref={(c) => { this._carousel = c; }}
              data={this.state.itemLocations}
              renderItem={this.renderCarouselItem}
              sliderWidth={Dimensions.get('window').width}
              itemWidth={300}
              onSnapToItem={this.onCarouselItemChanged}
            />
          </View>
          {this.state.scanScreen && 
            <ScanScreen 
              onClose={this.toggleScanner} 
              onScanned={this.scannerHandler}
            />}
          <Modal 
            animationType="slide" 
            visible={this.state.rentTime == null ? false : true}
          >
            <View style={styles.modal}>
              <Text>Your code is: {this.state.rentCode}</Text>
              <Text>You are riding</Text>
              <Text>{time}</Text>
              <Button title="Cancel" onPress={this.endRent}/>
            </View>
          </Modal>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  map: {
    flex: 1,
  },
  container: {
    width: "100%",
    height: "100%",
  },
  carousel: {
    position: 'absolute',
    bottom: '15%',
    alignSelf: 'center',
  },
  carouselItem: {
    alignItems: 'center',
    backgroundColor: 'snow',
    padding: 20,
    borderRadius: 20,
    marginTop: 100
  },
  carouselPhoto: {
    top: -100,
    width: 150,
    height: 150,
    position: "absolute"
  },
  locate:{
    position: 'absolute',
    bottom: '5%',
    right: '10%'
  },
  menu: {
    position: 'absolute',
    top: '5%',
    left: '5%',
  },
  button: {
    position: 'absolute',
    backgroundColor: "black",
    borderRadius: 5,
    bottom: '5%', 
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
});

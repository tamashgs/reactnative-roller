import * as React from 'react';
import LoadingScreen from './Loading'
import Map from './Map'
import { AsyncStorage, StyleSheet, Alert,} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { AuthContext } from './context.js';
import { SignInScreen } from './SignInScreen.js';
import { CreateAccount } from './SignUpScreen.js';
import { DrawerContent } from './DrawerContent.js';
import { ScanScreen } from './ScanScreen'
import { api } from './api.js';

const StackNav = createStackNavigator();
const DrawerNav = createDrawerNavigator();

const AuthStackOptions = {
  headerStyle: {
    backgroundColor: 'forestgreen',
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
  animationTypeForReplace: 'push'
  // state.isSignout ? 'pop' : 'push',
};

const AuthStackScreen = (props) => {
  return(
    <StackNav.Navigator>
      <StackNav.Screen 
                name="SignIn"
                component={SignInScreen}
                options={{...AuthStackOptions, title: 'Sign in',}}
      />
      <StackNav.Screen 
                name="SignUp"
                component={CreateAccount}
                options={{...AuthStackOptions, title: 'Sign Up'}}
      />
    </StackNav.Navigator>
  )
};

const MainStackScreen = (props) => {
  return(
    <DrawerNav.Navigator drawerContent={props => <DrawerContent {...props}/>}>
      <DrawerNav.Screen 
                name="Map"
                component={Map}
                options={{headerShown: false}}
      />
      <DrawerNav.Screen 
                name="Setting"
                component={ScanScreen}
                options={{...AuthStackOptions, title: 'Setting'}}
      />
      <DrawerNav.Screen 
                name="Profile"
                component={LoadingScreen}
                options={{...AuthStackOptions, title: 'Profile'}}
      />
      <DrawerNav.Screen 
          name="Scan"
          component={ScanScreen}
          options={{...AuthStackOptions, title: 'Scan'}}
      />
    </DrawerNav.Navigator>
  )
};

export default function Auth({ navigation }) {
  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'LOADING':
          return {
            ...prevState,
            isLoading: !prevState.isLoading,
          };
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            userToken: action.token,
            isLoading: false,
          };
        case 'SIGN_IN':
          return {
            ...prevState,
            isSignout: false,
            isLoading: false,
            userToken: action.token,
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            isLoading: false,
            isSignout: true,
            userToken: null,
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      userToken: null,
    }
  );

  async function getToken () {
    try {
      let token = await AsyncStorage.getItem("userToken");
      return token;
    } catch (error) {
      console.log("Something went wrong", error);
    }
  }

  //?
  React.useEffect(() => { //TODO - is valid token
    // Fetch the token from storage then navigate to our appropriate place
    const bootstrapAsync = async () => {
      getToken().then((token) => {
        if (token != null) { // is valid
          dispatch({ type: 'RESTORE_TOKEN', token: token });
        } else {
          dispatch({type: 'SIGN_OUT'});
        }
      })
      // After restoring token, we may need to validate it in production apps
      // This will switch to the App screen or Auth screen and this loading
      // screen will be unmounted and thrown away.
    };

    bootstrapAsync();
  }, []);

  const authContext = React.useMemo( 
    () => ({
      signIn: async data => {
        dispatch({ type: 'LOADING' });
        api.post('/login', {
          username: data.username,
          password: data.password
        }).then(response => {
          AsyncStorage.setItem("userToken", response.data.token);
          AsyncStorage.setItem("username", response.data.username);
          if (response.data.rentTime != null) 
            AsyncStorage.setItem("rentTime", response.data.rentTime);
          dispatch({ type: 'SIGN_IN', token: response.data.token })
        })
        .catch(error => {
          dispatch({ type: 'LOADING' });
          Alert.alert(error.response.data.error);
        });
      },
      signOut: () => {
        AsyncStorage.removeItem("userToken");
        AsyncStorage.removeItem("username");
        AsyncStorage.removeItem("rentTime");
        dispatch({ type: 'SIGN_OUT' })
      },
      signUp: async data => {
        console.log(data)
        data.openModal('load');
        api.post('/signup', {
          username: data.username,
          password: data.password,
          password2: data.password2,
          email: data.email,
        })
        .then((response) => {
          data.openModal('done');
        })
        .catch((error) => {
          data.closeModal();
          Alert.alert(error.response.data.error);
        });
      },
    }), []
  );

  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
          { state.isLoading ? ( 
            <LoadingScreen/> 
          ) : 
          state.isSignout === true ? ( 
            <AuthStackScreen/>
          ) : (
            <MainStackScreen/>
          )}
      </NavigationContainer>
    </AuthContext.Provider>
  );
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  container: {
    flex: 1,
    alignItems: 'center',
  },
  input: {
    backgroundColor: 'grey',
    height: '10%',
    width: '80%',
    margin: 5,
    borderRadius: 30,
    textAlign: 'center'
  },
  button: {
    height: '10%',
    width: '80%',
    borderRadius: 80,
    margin: 5,
  }
});

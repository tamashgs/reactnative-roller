import { createStore, combineReducers } from 'redux';

import reducer from './reducers/countReducer'

const rootReduces = combineReducers({
    count: reducer
});

const store = createStore(rootReduces)

export default store

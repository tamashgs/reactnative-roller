import { COUNTER_CHANGE } from '../constant/index';

export function changeCount(count) {
    return {
        type: COUNTER_CHANGE,
        payload: count
    }
}
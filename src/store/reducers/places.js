import { addPlace, selectPlace, deletePlace, deselectPlace } from '../actions/index'
import { ADD_PLACE, DELETE_PLACE, SELECT_PLACE, DESELECT_PLACE } from '../actions/actionTypes';


const initialState = {
    places: [],
    selectedPlace: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        default:
            case ADD_PLACE:
                return {
                    ...state,
                    // app.js code
                };
            case DELETE_PLACE:
                return {
                    //appjs
                };
            case SELECT_PLACE:
                return {

                };
            case DESELECT_PLACE:
                return {
                      
                };
            return state;
    }
};

export default reducer;
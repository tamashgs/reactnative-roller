import * as React from 'react';
import { View, Text, Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';

function Feed({ navigation }) {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Feed Screen</Text>
      <Button title="Open drawer" onPress={() => navigation.openDrawer()} />
      <Button title="Toggle drawer" onPress={() => navigation.toggleDrawer()} />
    </View>
  );
}

function Notifications() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Notifications Screen</Text>
    </View>
  );
}

function CustomDrawerContent(props) {
  return (
    <DrawerContentScrollView {...props}>
      <DrawerItemList {...props} />
      <DrawerItem
        label="Close drawer"
        onPress={() => props.navigation.closeDrawer()}
      />
      <DrawerItem
        label="Toggle drawer"
        onPress={() => props.navigation.toggleDrawer()}
      />
    </DrawerContentScrollView>
  );
}

const Drawer = createDrawerNavigator();

function MyDrawer() {
  return (
    <Drawer.Navigator drawerContent={props => <CustomDrawerContent {...props} />}>
      <Drawer.Screen name="Feed" component={Feed} />
      <Drawer.Screen name="Notifications" component={Notifications} />
    </Drawer.Navigator>
  );
}

export default function App() {
  return (
    <NavigationContainer>
      <MyDrawer />
    </NavigationContainer>
  );
}


// redux
// import { Provider } from 'react-redux';
// import store from './src/store/configureStore';
import React, { Component } from "react"
import {View, Text} from 'react-native'
import Map from './src/screens/Map'
import Start from './src/screens/Start'

import { NavigationContainer } from '@react-navigation/native'
import { createDrawerNavigator } from '@react-navigation/drawer'
import { createStackNavigator } from '@react-navigation/stack'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'


const drawer = createDrawerNavigator();
const stack = createStackNavigator();
const bottomTab = createMaterialBottomTabNavigator();
const topTab = createMaterialTopTabNavigator();

const homeStack = () => {
  return(
    <stack.Navigator>
      <stack.Screen name="1" component={Start}/>
      <stack.Screen name="2" component={Start}/>
    </stack.Navigator>
  );
}

const App = () => (
  <NavigationContainer>
    <drawer.Navigator>
      {/* <View>
        <Text>asd</Text>
      </View> */}
      <drawer.Screen name="Home" children={homeStack}/>
      <drawer.Screen name="A" component={Map}/>
      <drawer.Screen name="B" component={Start}/>
    </drawer.Navigator>
  </NavigationContainer>
);

export default App;